@extends('template')
@section('content')
<div class="card-body">
	<h3>Data Pegawai</h3>
	<br/>
	<table class="table table-bordered">
		<form action="{{ ($type=="edit") ? '/mahasiswa/edit/ubah' : '/mahasiswa/buat/tambah' }}" method="post">
			{{ csrf_field() }}
			<tr>
				<td>
					Nama
				</td>
				<td>
					<input type="hidden" name="id" value="{{ ($type=="edit") ? $data->id_mahasisawa : '' }}"> <br/>
					<input type="text" name="nama" required="required" value="{{ ($type=="edit") ? $data->mm_mahasiswa : '' }}"> <br/>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="cen">
					<input type="submit" value="Simpan Data">
				</td>
			</tr>
		</form>
	</table>
</div>
@endsection