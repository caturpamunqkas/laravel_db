@extends('template')
@section('content')
<div class="card-body">
	@if(\Session::has('alert-success'))
        <div class="alert alert-success">
            <div>{{Session::get('alert-success')}}</div>
        </div>
    @endif
	<h3>Data Mahasiswa</h3>
	<p>Cari Data Mahasiswa :</p>
	<form action="/mahasiswa/cari" method="GET" class="form-inline">
		<input class="form-control" type="text" name="cari" placeholder="Cari Mahasiswa .." value="">
		<input class="btn btn-primary ml-3" type="submit" value="CARI">
	</form>
	<br>
	<a style="margin-bottom: 10px" class="btn btn-success btn-sm" href="/mahasiswa/buat">Tambah</a>
	<table class="table table-bordered">
		<tbody>
			<tr>
				<th>Nama</th>
				<th>Opsi</th>
			</tr>
		</tbody>
		@foreach($mahasiswa as $d)
			<tr>
				<td>{{$d->mm_mahasiswa}}</td>
				<td>
					<a class="btn btn-warning btn-sm" href="/mahasiswa/edit/{{$d->id_mahasisawa}}">Edit</a>
					<a class="btn btn-danger btn-sm" href="/mahasiswa/hapus/{{$d->id_mahasisawa}}">Hapus</a>
				</td>
			</tr>
		@endforeach
	</table>
	<br>
</div>
@endsection