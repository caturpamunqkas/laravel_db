<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
</head>
<body>
	<div class="container">
		<div class="card">
			<div class="card-body">
				<section class="main-section">
			        <!-- Add Your Content Inside -->
			        <div class="content">
			            <!-- Remove This Before You Start -->
			            <h1>Login Page</h1>
			            <hr>
			            @if(\Session::has('alert'))
			                <div class="alert alert-danger">
			                    <div>{{Session::get('alert')}}</div>
			                </div>
			            @endif
			            @if(\Session::has('alert-success'))
			                <div class="alert alert-success">
			                    <div>{{Session::get('alert-success')}}</div>
			                </div>
			            @endif
			            <form action="/user/login" method="post">
			                {{ csrf_field() }}
			                <div class="form-group">
			                    <label for="username">Username:</label>
			                    <input type="text" class="form-control" id="username" name="username">
			                </div>
			                <div class="form-group">
			                    <label for="alamat">Password:</label>
			                    <input type="password" class="form-control" id="password" name="password"></input>
			                </div>
			                <div class="form-group">
			                    <button type="submit" class="btn btn-md btn-primary">Login</button>
			                    <a href="{{url('register')}}" class="btn btn-md btn-warning">Register</a>
			                </div>
			            </form>
			        </div>
			        <!-- /.content -->
			    </section>
			</div>
		</div>
	</div>
</body>
</html>