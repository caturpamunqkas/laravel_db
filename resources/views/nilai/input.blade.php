@extends('template')
@section('content')
<div class="card-body">
	<h3>Data Pegawai</h3>
	<br/>
	<table class="table table-bordered">
		<form action="{{ ($type=="edit") ? '/nilai/edit/ubah' : '/nilai/buat/tambah' }}" method="post">
			{{ csrf_field() }}
			<tr>
				<td>
					Nama Mahasiswa
				</td>
				<td>
					<input type="hidden" name="id" value="{{ ($type=="edit") ? $data->id_niali : '' }}"> <br/>
					<div class="form-group">
						<select class="form-control" id="id_mahasiswa" name="id_mahasiswa">
					    	@foreach($siswa as $d)
						    	<option value="{{$d->id_mahasisawa}}">{{ $d->mm_mahasiswa." - ".$d->id_mahasisawa }}</option>
					    	@endforeach
					    </select>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					Mata Kuliah
				</td>
				<td>
					<div class="form-group">
						<select class="form-control" id="id_matkul" name="id_matkul">
					    	@foreach($matkul as $d)
						    	<option value="{{$d->id_mataKuliah}}">{{ $d->nm_mataKuliah." - ".$d->id_mataKuliah }}</option>
					    	@endforeach
					    </select>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					Nilai
				</td>
				<td>
					<input type="text" name="nilai" required="required" value="{{ ($type=="edit") ? $data->nilai : '' }}"> <br/>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="cen">
					<input type="submit" value="Simpan Data">
				</td>
			</tr>
		</form>
	</table>
</div>
@endsection