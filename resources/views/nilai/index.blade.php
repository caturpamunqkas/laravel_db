@extends('template')
@section('content')
<div class="card-body">
	@if(\Session::has('alert-success'))
        <div class="alert alert-success">
            <div>{{Session::get('alert-success')}}</div>
        </div>
    @endif
	<h3>Data Nilai</h3>
	<a style="margin-bottom: 10px" class="btn btn-success btn-sm" href="/nilai/buat">Tambah</a>
	<table class="table table-bordered">
		<tbody>
			<tr>
				<th>Nama Mahasiswa</th>
				<th>Mata KUliah</th>
				<th>Nilai</th>
				<th>Opsi</th>
			</tr>
		</tbody>
		@foreach($data as $d)
			<tr>
				<td>{{$d->mahasiswa->mm_mahasiswa}}</td>
				<td>{{$d->matkul->nm_mataKuliah}}</td>
				<td>{{$d->nilai}}</td>
				<td>
					<a class="btn btn-warning btn-sm" href="/nilai/edit/{{$d->id_niali}}">Edit</a>
					<a class="btn btn-danger btn-sm" href="/nilai/hapus/{{$d->id_niali}}">Hapus</a>
				</td>
			</tr>
		@endforeach
	</table>
	<br>
</div>
@endsection