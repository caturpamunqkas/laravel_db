@extends('template')
@section('content')
<div class="card-body">
	@if(\Session::has('alert-success'))
        <div class="alert alert-success">
            <div>{{Session::get('alert-success')}}</div>
        </div>
    @endif
	<h3>Data Mata Kuliah</h3>
	<a style="margin-bottom: 10px" class="btn btn-success btn-sm" href="/matkul/buat">Tambah</a>
	<table class="table table-bordered">
		<tbody>
			<tr>
				<th>Mata Kuliah</th>
				<th>Opsi</th>
			</tr>
		</tbody>
		@foreach($mahasiswa as $d)
			<tr>
				<td>{{$d->nm_mataKuliah}}</td>
				<td>
					<a class="btn btn-warning btn-sm" href="/matkul/edit/{{$d->id_mataKuliah}}">Edit</a>
					<a class="btn btn-danger btn-sm" href="/matkul/hapus/{{$d->id_mataKuliah}}">Hapus</a>
				</td>
			</tr>
		@endforeach
	</table>
	<br>
</div>
@endsection