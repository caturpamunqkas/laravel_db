<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NilaiModel extends Model
{	
	protected $table="tbl_nilai";
	protected $primaryKey="id_niali";

    public function mahasiswa()
	{
		return $this->hasOne('App\MahasiswaModel', 'id_mahasisawa', 'id_mahasiswa');
	}

	public function matkul()
	{
		return $this->hasOne('App\MatkulModel', 'id_mataKuliah', 'id_matkul');
	}
}
