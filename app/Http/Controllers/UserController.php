<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    public function index()
    {
    	if(Session::get('login')){
            return redirect('/mahasiswa');
        }else{
	    	return view('home.index');
        }
    }

    public function login(Request $request)
    {
    	$data = DB::table('tbl_users')->where('username', $request->username)->count();

    	if (!$data==0) {
    		$cek = DB::table('tbl_users')->where('username', $request->username)->first();

    		if ($cek->password == $request->password) {
	    		// return redirect('/')->with('alert','Data Sama');
	    		Session::put('name',$cek->username);
                Session::put('login',TRUE);
                return redirect('/mahasiswa');
    		}else{
    			return redirect('/')->with('alert','Password Salah');
    		}

    	}else{
    		return redirect('/')->with('alert','Username Tidak Ditemukan');
    	}
    }

    public function logout(){
        Session::flush();
        return redirect('/')->with('alert','Kamu sudah logout');
    }
}
