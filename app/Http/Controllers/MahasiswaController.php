<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\MahasiswaModel;

class MahasiswaController extends Controller
{

	public function __construct()
	{
        // parent::__construct();
		if(!Session::get('login')){
            return redirect('/')->with('alert','Kamu harus login dulu');
        }
	}

    public function index()
    {
    	if(!Session::get('login')){
            return redirect('/')->with('alert','Kamu harus login dulu');
        }
        
    	$data['type'] = 'mahasiswa';
    	$data['mahasiswa'] = MahasiswaModel::all();
 
    	// mengirim data pegawai ke view index
    	return view('mahasiswa.index', $data);
 
    }

    public function buat()
    {
        if(!Session::get('login')){
            return redirect('/')->with('alert','Kamu harus login dulu');
        }

    	$type = 'buat';

    	return view('mahasiswa.input', ['type' => $type]);
    }

    public function tambah(Request $request)
    {
        if(!Session::get('login')){
            return redirect('/')->with('alert','Kamu harus login dulu');
        }

        $data = new MahasiswaModel;
        $data->mm_mahasiswa = $request->nama;
        $data->save();

		return redirect('/mahasiswa')->with('alert-success', 'Data Berhasil Di Tambah');
    }

    public function edit($id)
    {
        if(!Session::get('login')){
            return redirect('/')->with('alert','Kamu harus login dulu');
        }

        $data['type'] = 'edit';
    	$data['data'] = MahasiswaModel::where('id_mahasisawa',$id)->first();

    	return view('mahasiswa.input', $data);
    	// return $data;
    }

    public function ubah(Request $request)
    {
        if(!Session::get('login')){
            return redirect('/')->with('alert','Kamu harus login dulu');
        }

		MahasiswaModel::where('id_mahasisawa', $request->id)->update([
			'mm_mahasiswa' => $request->nama
		]);
		
		return redirect('/mahasiswa')->with('alert-success', 'Data Berhasil Di Ubah');
    }

    public function hapus($id)
    {
        if(!Session::get('login')){
            return redirect('/')->with('alert','Kamu harus login dulu');
        }
        
    	MahasiswaModel::where('id_mahasisawa',$id)->delete();
		
		// alihkan halaman ke halaman pegawai
		return redirect('/mahasiswa')->with('alert-success', 'Data Berhasil Di Hapus');
    }
}
