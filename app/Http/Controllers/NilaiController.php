<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use App\MatkulModel;
use App\MahasiswaModel;
use App\NilaiModel;

class NilaiController extends Controller
{
    public function index()
    {

    	if(!Session::get('login')){
            return redirect('/')->with('alert','Kamu harus login dulu');
        }
        
    	$data['type'] = 'nilai';
    	$data['data'] = NilaiModel::with('mahasiswa', 'matkul')->get();
 	
    	// echo $data['data'];

    	// mengirim data pegawai ke view index
    	return view('nilai.index', $data);
 
    }

    public function buat()
    {
    	if(!Session::get('login')){
            return redirect('/')->with('alert','Kamu harus login dulu');
        }

    	$data['type'] = 'buat';
    	$data['siswa'] = MahasiswaModel::all();
    	$data['matkul'] = MatkulModel::all();

    	return view('nilai.input', $data);
    }

    public function tambah(Request $request)
    {
    	if(!Session::get('login')){
            return redirect('/')->with('alert','Kamu harus login dulu');
        }

        $data = new NilaiModel;
        $data->id_mahasiswa = $request->id_mahasiswa;
        $data->id_matkul 	= $request->id_matkul;
        $data->nilai 		= $request->nilai;
        $data->save();

		return redirect('/nilai')->with('alert-success', 'Data Berhasil Di Tambah');
    }

    public function edit($id)
    {

    	if(!Session::get('login')){
            return redirect('/')->with('alert','Kamu harus login dulu');
        }

        $data['type'] = 'edit';
    	$data['data'] = NilaiModel::where('id_niali',$id)->first();
    	$data['siswa'] = MahasiswaModel::all();
    	$data['matkul'] = MatkulModel::all();

    	return view('nilai.input', $data);
    	// return $data;
    }

    public function ubah(Request $request)
    {
    	if(!Session::get('login')){
            return redirect('/')->with('alert','Kamu harus login dulu');
        }

		NilaiModel::where('id_niali', $request->id)->update([
			'id_mahasiswa' => $request->id_mahasiswa,
			'id_matkul' => $request->id_matkul,
			'nilai' => $request->nilai
		]);
		
		return redirect('/nilai')->with('alert-success', 'Data Berhasil Di Ubah');
    }

    public function hapus($id)
    {
    	if(!Session::get('login')){
            return redirect('/')->with('alert','Kamu harus login dulu');
        }

    	MatkulModel::where('id_niali',$id)->delete();
		
		// alihkan halaman ke halaman pegawai
		return redirect('/nilai')->with('alert-success', 'Data Berhasil Di Hapus');
    }
}
