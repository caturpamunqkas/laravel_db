<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MatkulModel extends Model
{
    protected $table="tbl_mataKuliah";
	protected $primaryKey="id_mataKuliah";
}
