<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'UserController@index');
Route::post('/user/login', 'UserController@login');
Route::get('/logout', 'UserController@logout');

Route::get('/mahasiswa', 'MahasiswaController@index');
Route::get('/mahasiswa/buat', 'MahasiswaController@buat');
Route::post('/mahasiswa/buat/tambah', 'MahasiswaController@tambah');
Route::get('/mahasiswa/edit/{id}', 'MahasiswaController@edit');
Route::post('/mahasiswa/edit/ubah', 'MahasiswaController@ubah');
Route::get('/mahasiswa/hapus/{id}', 'MahasiswaController@hapus');

Route::get('/matkul', 'MatkulController@index');
Route::get('/matkul/buat', 'MatkulController@buat');
Route::post('/matkul/buat/tambah', 'MatkulController@tambah');
Route::get('/matkul/edit/{id}', 'MatkulController@edit');
Route::post('/matkul/edit/ubah', 'MatkulController@ubah');
Route::get('/matkul/hapus/{id}', 'MatkulController@hapus');

Route::get('/nilai', 'NilaiController@index');
Route::get('/nilai/buat', 'NilaiController@buat');
Route::post('/nilai/buat/tambah', 'NilaiController@tambah');
Route::get('/nilai/edit/{id}', 'NilaiController@edit');
Route::post('/nilai/edit/ubah', 'NilaiController@ubah');
Route::get('/nilai/hapus/{id}', 'NilaiController@hapus');
